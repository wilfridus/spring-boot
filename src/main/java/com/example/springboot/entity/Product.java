package com.example.springboot.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Product {

	@Id @GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid", strategy="uuid2")
	private String id;
	
	@NotNull @NotEmpty @Size(max=00)
	private String code;
	
	@NotNull @NotEmpty
	private String name;
	
	public Product() {
		// TODO Auto-generated constructor stub
		
	}

}
