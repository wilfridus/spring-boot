package com.example.springboot.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.springboot.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}
