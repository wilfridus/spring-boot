package com.example.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.dao.ProductDao;
import com.example.springboot.entity.Product;

@RestController @RequestMapping("/api/product")
public class ProductAPIController {

	@Autowired
	private ProductDao productDao;
	
	public ProductAPIController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/")
	public Page<Product> findProducts(Pageable page){
		return productDao.findAll(page);
	}
}
